class AddDesignToBriefs < ActiveRecord::Migration[5.2]
  def change
    add_column :briefs, :design, :string
  end
end
