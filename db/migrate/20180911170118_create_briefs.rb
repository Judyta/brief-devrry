class CreateBriefs < ActiveRecord::Migration[5.2]
  def change
    create_table :briefs do |t|
      t.string :company
      t.string :brand
      t.text :description
      t.string :email
      t.string :contact
      t.text :offers
      t.text :competition
      t.text :project
      t.text :structure
      t.string :group
      t.text :budget
      t.date :daadline
      t.text :example
      t.text :featured
      t.text :additional

      t.timestamps
    end
  end
end
