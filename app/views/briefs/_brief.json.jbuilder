json.extract! brief, :id, :company, :brand, :description, :email, :contact, :offers, :competition, :project, :structure, :group, :budget, :daadline, :example, :featured, :additional, :created_at, :updated_at
json.url brief_url(brief, format: :json)
