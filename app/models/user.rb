class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :validatable
  has_one :role
  has_many :briefs
end