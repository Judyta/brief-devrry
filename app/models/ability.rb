class Ability
  include CanCan::Ability
  user ||= User.new(role: "guest") # not logged in
  if user.role? :admin
    can :manage, :all
  end
  if user.role? :guest
    can :create, Brief
  end
end