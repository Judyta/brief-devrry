Rails.application.routes.draw do
  devise_for :users
  root to: "briefs#new"
  resources :briefs
  get "/pages/:page" => "pages#show"
end
